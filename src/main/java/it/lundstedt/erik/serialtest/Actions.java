package it.lundstedt.erik.serialtest;

import it.lundstedt.erik.menu.Menu;
import jssc.SerialPort;
import jssc.SerialPortException;

public class Actions
{
SerialPort serial;

public Actions(String port) {
		
		serial = new SerialPort(port);
	

	
}

public void print()
{
	try {
		serial.openPort();
		Thread.sleep(Settings.delay);
		System.out.println("connected to port "+serial.getPortName());
		
		serial.writeString(Menu.getInput("data to send"));
		
	} catch (SerialPortException | InterruptedException e) {
		e.printStackTrace();
	}
}


}